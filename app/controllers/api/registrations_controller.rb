class Api::RegistrationsController < Devise::RegistrationsController

  respond_to :json

  def create
    build_resource(sign_up_params)
    if resource.save
      sign_in(resource, :store => false)
      render :status => 200,
             :json => { :success => true,
                        :info => t("devise.registrations.signed_up"),
                        :data => { :user => resource } }
    else
      render :status => :unprocessable_entity,
             :json => { :success => false,
                        :info => resource.errors.full_messages,
                        :data => {} }
    end
  end

  def sign_up_params
    params.require(:user).permit(:email, :password, :password_confirmation, :mobile, :gender)
  end

end