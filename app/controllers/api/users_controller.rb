module Api
  class UsersController < Api::BaseController

    private

    def user_params
      params.require(:user).permit(:user)
    end

    def query_params
      # this assumes that an album belongs to an artist and has an :user_id
      # allowing us to filter by this
      params.permit(:email, :gender, :mobile, :avatar)
    end

  end
end